// IRemoteServiceCallback.aidl
package com.example.psobczynski.aidlpracticedbservice;


// Declare any non-default types here with import statements
import com.example.psobczynski.Product;

 interface IRemoteServiceCallback {
   oneway void addProductResult(in Product product);
   oneway void delProductResult(in long result, in int position);
}
