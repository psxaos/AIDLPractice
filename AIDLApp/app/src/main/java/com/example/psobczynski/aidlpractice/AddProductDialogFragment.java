package com.example.psobczynski.aidlpractice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.psobczynski.Product;

/**
 * Created by p.sobczynski on 2017-09-22.
 */

public class AddProductDialogFragment extends DialogFragment {

    public static String TAG = "AddProductDialogFragment";
    private EditText etName, etPrice, etCountry, etDelivery;
    private String name, country;
    private int price;
    private long delivery;
    //instance of the interface to use for delivering events
    ProductDialogListener mDialogListener;
    public Product product;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.add_product_dialog, null);

        etName = (EditText) view.findViewById(R.id.etName);
        etPrice = (EditText) view.findViewById(R.id.etPrice);
        etCountry = (EditText) view.findViewById(R.id.etCountry);
        etDelivery = (EditText) view.findViewById(R.id.etDelivery);


        builder.setView(view);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name = etName.getText().toString();
                String tempPrice = etPrice.getText().toString();
                if (tempPrice != null && !tempPrice.equals("")) {
                    price = Integer.parseInt(tempPrice);
                } else {
                    price = 0;
                }

                country = etCountry.getText().toString();
                String tempDelivery = etDelivery.getText().toString();
                if (tempDelivery != null && !tempDelivery.equals("")) {
                    delivery = Long.parseLong(tempDelivery);
                } else {
                    delivery = 0;
                }
                long idx = -1L; //TODO: Remove, not used, just a value

                //TODO: validate input fields
                //TODO: define addProduct method call it in Main activity

                //addProduct(new Product(name, price, country, delivery));
                product = new Product(name, price, country, delivery, idx);
                try {
                    mDialogListener.onDialogPositiveClick(product);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AddProductDialogFragment.this.getDialog().cancel();
            }
        });

        return builder.create();
    }


    //Passing data to MainActivity for further processing
    public interface ProductDialogListener {
        public void onDialogPositiveClick(Product product) throws RemoteException;

        public void onDialogNegativeClick(Product product);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Check if host activity implements the callback interface

        try {
            mDialogListener = (ProductDialogListener) context;
        } catch (ClassCastException e) {
            //Thrown when Activity does not implement the listener
            throw new ClassCastException(context.toString() + " must implement ProductDialogListener");

        }

    }

}
