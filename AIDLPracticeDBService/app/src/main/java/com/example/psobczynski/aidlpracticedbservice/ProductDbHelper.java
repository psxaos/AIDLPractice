package com.example.psobczynski.aidlpracticedbservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.example.psobczynski.Product;

/**
 * Created by p.sobczynski on 2017-09-13.
 */

public class ProductDbHelper extends SQLiteOpenHelper {

    public static final String TAG = "ProductDbHelper";
    public static final String DB_NAME = "product_db";
    public static final int DB_VERSION = 2;


    public ProductDbHelper(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
        Log.d("ProductDbHelper", "ProductDbHelper: Constructor");
    }

    //SQL queries definition
    public static final String CREATE_DB_QUERY = "CREATE TABLE " + ProductContract.ProductRecord.TABLE_NAME + " ( "
            + ProductContract.ProductRecord._ID + " INTEGER PRIMARY KEY, "
            + ProductContract.ProductRecord.NAME + " TEXT, "
            + ProductContract.ProductRecord.PRICE + " INTEGER, "
            + ProductContract.ProductRecord.COUNTRY + " TEXT, "
            + ProductContract.ProductRecord.DELVERY + " INTEGER)";

    public static final String DROP_TABLE_QUERY = "DROP TABLE IF EXIST " + ProductContract.ProductRecord.TABLE_NAME + ";";

    public static final String GET_ROW_BT_ID = "ProductContract.ProductRecord._ID " + " = ' ?s '";


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("ProductDbHelper", "onCreate: ");
        db.execSQL(CREATE_DB_QUERY);
        Log.d(TAG, " Table has been created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_QUERY);
        Log.d(TAG, " Database has been updated");
    }

    public long addProduct(Product product, SQLiteDatabase db) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(ProductContract.ProductRecord.NAME, product.getName());
        contentValues.put(ProductContract.ProductRecord.PRICE, product.getPrice());
        contentValues.put(ProductContract.ProductRecord.COUNTRY, product.getCountry());
        contentValues.put(ProductContract.ProductRecord.DELVERY, product.getDelivery());

        long idx = db.insert(ProductContract.ProductRecord.TABLE_NAME, null, contentValues);
        Log.d("ProductDbHelper", "addProduct: Data row inserted");
        return idx;
    }

    public Cursor getData(SQLiteDatabase db) {
        String[] result = {ProductContract.ProductRecord._ID, ProductContract.ProductRecord.NAME, ProductContract.ProductRecord.PRICE, ProductContract.ProductRecord.COUNTRY, ProductContract.ProductRecord.DELVERY};
        Cursor cursor = db.query(ProductContract.ProductRecord.TABLE_NAME, result, null, null, null, null, null);
        return cursor;
    }

    public long deleteProduct(long id, SQLiteDatabase db) {

        String tableName = ProductContract.ProductRecord.TABLE_NAME;
        String whereClause = "_ID=? ";
        String[] whereArgs = new String[]{String.valueOf(id)};
        int tempIdx = db.delete(tableName, whereClause, whereArgs);
        long idx = tempIdx;
        return idx;
    }

    public Cursor getRowById(long id, SQLiteDatabase db) {
        String[] result = {ProductContract.ProductRecord._ID,
                ProductContract.ProductRecord.NAME,
                ProductContract.ProductRecord.PRICE,
                ProductContract.ProductRecord.COUNTRY,
                ProductContract.ProductRecord.DELVERY};
        String tableName = ProductContract.ProductRecord.TABLE_NAME;
        String[] where = new String[]{String.valueOf(id)};
        Cursor cursor = db.query(tableName, result, GET_ROW_BT_ID, where, null, null, null);

        return cursor;
    }

}
