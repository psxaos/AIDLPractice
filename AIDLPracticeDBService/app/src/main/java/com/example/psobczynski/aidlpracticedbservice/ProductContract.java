package com.example.psobczynski.aidlpracticedbservice;

import android.provider.BaseColumns;

/**
 * Created by p.sobczynski on 2017-09-13.
 */

public class ProductContract{

    public ProductContract(){
    }

    public static class ProductRecord implements BaseColumns {
        public static final String TABLE_NAME = "product_details";
        public static final String NAME = "name";
        public static final String PRICE = "price";
        public static final String COUNTRY = "country";
        public static final String DELVERY = "delivery";
    }



}
