package com.example.psobczynski.aidlpracticedbservice;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Debug;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.psobczynski.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by p.sobczynski on 2017-09-13.
 */

public class AIDLPracticeDBService extends Service {

    public static final String TAG = "AIDLPracticeDBService";
    ProductDbHelper mProductDbHelper;
    SQLiteDatabase db;
    ArrayList<Product> arrayList = new ArrayList<>();

    IRemoteServiceCallback.Stub mRemoteCallback;

    public AIDLPracticeDBService() {

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "AIDLPracticeDBService: enter method: onBind()");
        //Debug.waitForDebugger();
        //open the connection to the SQLite DB
        mProductDbHelper = new ProductDbHelper(this);
        db = mProductDbHelper.getWritableDatabase();
        Log.d(TAG, "onStartCommand(): db connection open " + db);

        return mBinder;

    }

    private final IProductDBConnect.Stub mBinder = new IProductDBConnect.Stub() {

        @Override
        public void addProduct(Product p, IRemoteServiceCallback callback) throws RemoteException {
            Log.d("AIDLPracticeDBService", "addProduct: ");
            long idx = mProductDbHelper.addProduct(p, db);
            p.setIndex(idx); // add index assinged in db and set it in product index field

            callback.addProductResult(p);


        }

        @Override
        public void deleteProduct(long idx, int position, IRemoteServiceCallback callback) throws RemoteException {
            Log.d("AIDLPracticeDBService", "deleteProduct: ");
            long deleteResult = mProductDbHelper.deleteProduct(idx, db);
            callback.delProductResult(deleteResult, position);

        }

        @Override
        public List<Product> getAllProducts() throws RemoteException {

            Cursor cursor = mProductDbHelper.getData(db);
            // Debug.waitForDebugger();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Product product = new Product(
                        cursor.getString(1),
                        cursor.getInt(2),
                        cursor.getString(3),
                        cursor.getLong(4),
                        cursor.getLong(0));

                arrayList.add(product);
                cursor.moveToNext();
            }
            cursor.close();
            return arrayList;
        }

    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "AIDLPracticeDBService.onDestroy()");
        mProductDbHelper.close();
        Log.d(TAG, "onDestroy(): db connection closed");
        super.onDestroy();
    }

}