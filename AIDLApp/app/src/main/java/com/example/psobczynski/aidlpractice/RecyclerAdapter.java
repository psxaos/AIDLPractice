package com.example.psobczynski.aidlpractice;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.psobczynski.Product;

import java.util.ArrayList;

/**
 * Created by p.sobczynski on 2017-09-14.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    ArrayList<Product> arrayList = new ArrayList<>();

    RecyclerAdapter(ArrayList<Product> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_layout, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Product product = arrayList.get(position);
        holder.name.setText(product.getName());
        holder.price.setText(Integer.toBinaryString(product.getPrice()));
        holder.country.setText(product.getCountry());
        holder.delivery.setText(Long.toHexString(product.getDelivery())); //TODO: display using  the date format
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView name, price, country, delivery;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            country = (TextView) itemView.findViewById(R.id.country);
            delivery = (TextView) itemView.findViewById(R.id.delivery);


        }
    }
}
