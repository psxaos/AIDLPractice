// IProductDBConnect.aidl
package com.example.psobczynski.aidlpracticedbservice;

// Declare any non-default types here with import statements
import com.example.psobczynski.Product;
import com.example.psobczynski.aidlpracticedbservice.IRemoteServiceCallback;

interface IProductDBConnect {

       oneway void addProduct(in Product product, in IRemoteServiceCallback callback);
       oneway void deleteProduct(in long idx, in int position, in IRemoteServiceCallback callback);
   List<Product> getAllProducts();
}
