package com.example.psobczynski.aidlpractice;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.psobczynski.Product;
import com.example.psobczynski.aidlpracticedbservice.IProductDBConnect;
import com.example.psobczynski.aidlpracticedbservice.IRemoteServiceCallback;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AddProductDialogFragment.ProductDialogListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    TextView tvTest;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Product> arrayList = new ArrayList<>();
    IProductDBConnect aidlService;
    ProductConnection mServiceConnection;

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity", "onCreate: entered method");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvTest = (TextView) findViewById(R.id.tvTest);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        //Bind to the service and read data
        initService();
        Log.d(TAG, "onCreate(): initService() executed");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //start remote service method
                Log.d(TAG, "fab.setOnClickListener() :OnClick()");

                AddProductDialogFragment dialogFragment = new AddProductDialogFragment();
                dialogFragment.show(getFragmentManager(), dialogFragment.getClass().getSimpleName());

                //TODO: temp check the AIDL implementation by adding two digits and display it in the snackbar
                Snackbar.make(view, "a kuku", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    @Override
    public void onDialogPositiveClick(Product product) throws RemoteException {
        Log.d(TAG, "MainActivity: onDialogPositiveClick()");
        Log.d(TAG, (String) product.getName());
        Log.d(TAG, String.valueOf(product.getPrice()));
        Log.d(TAG, String.valueOf(product.getCountry()));
        Log.d(TAG, String.valueOf(product.getDelivery()));
        aidlService.addProduct(product, mCallback);


    }

    @Override
    public void onDialogNegativeClick(Product product) {
        Log.d(TAG, "MainActivity: onDialogNegativeClick()");
    }


    /**
     * This class represents service connection. It type-casts bound-stub implementation of the service to our AIDL interface
     */
    class ProductConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder boundService) {
            aidlService = IProductDBConnect.Stub.asInterface(boundService);
            Log.d("ProductConnection", "onServiceConnected: connected");
            Toast.makeText(MainActivity.this, "AIDL Service Connected", Toast.LENGTH_LONG).show();
            fab.setEnabled(true);

            //Read data from DB
            try {
                arrayList = (ArrayList<Product>) aidlService.getAllProducts();
                Log.d("ProductConnection", "onServiceConnected: data from DB written into arrayList");
            } catch (Exception e) {
                Log.d("ProductConnection", "onServiceConnected: Error reading data from DB");
                e.printStackTrace();

            }

            //recyclerView setup
            recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            layoutManager = new LinearLayoutManager(MainActivity.this);
            recyclerView.setLayoutManager(layoutManager);

            //let's animate add/remove items using default animator
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            adapter = new RecyclerAdapter(arrayList);
            recyclerView.setAdapter(adapter);

            //setup swipe
            ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    Log.d("ProductConnection", "onSwiped: swiped left");
                    Toast.makeText(MainActivity.this, "onSwiped LEFT", Toast.LENGTH_LONG).show();
                    int position = viewHolder.getAdapterPosition();

                    long idx = arrayList.get(position).getIndex();
                    try {
                        aidlService.deleteProduct(idx, position, mCallback);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Operation not successful.", Toast.LENGTH_LONG).show();
                    }


                }
            };


            ItemTouchHelper itemTouchHelpoer = new ItemTouchHelper(simpleItemTouchCallback);
            itemTouchHelpoer.attachToRecyclerView(recyclerView);


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            aidlService = null;
            fab.setEnabled(false);
            Log.d(TAG, "onServiceDisconnected(): Disconnected");
            Toast.makeText(MainActivity.this, "AIDL Service Disconnected", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Binds MainActivity to remote service
     */
    private void initService() {
        Log.i(TAG, "initService()");
        mServiceConnection = new ProductConnection();
        Intent intent = new Intent();
        intent.setClassName("com.example.psobczynski.aidlpracticedbservice", "com.example.psobczynski.aidlpracticedbservice.AIDLPracticeDBService");
        boolean serviceExists = bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        Log.d("MainActivity", "initService: " + serviceExists);

    }

    /**
     * This method unbinds this activity from the service.
     */
    private void releaseService() {
        unbindService(mServiceConnection);
        mServiceConnection = null;
        Log.i(TAG, "releaseService(): service unbound");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Dealing with callbacks from the remote service

    IRemoteServiceCallback mCallback = new IRemoteServiceCallback.Stub() {
        @Override
        public void addProductResult(final Product newProduct) throws RemoteException {
            //TODO: if idx != -1 update ArrayList and RecyclerView otherwise display error message
            //TODO: runOnUIThread()...
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("MainActivity", "run: callback received with product data");
                    if (newProduct.getIndex() != -1) {
                        //add new Product to  arrayList
                        arrayList.add(newProduct);
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.getLayoutManager().scrollToPosition(arrayList.size() - 1);
                    } else {
                        Toast.makeText(MainActivity.this, "Error. Product has not been added to the database.", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        @Override
        public void delProductResult(final long result, final int position) throws RemoteException {

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (result > 0) { //arg1 = result
                        //delete record from array list if item was deleted in db
                        arrayList.remove(position);
                        recyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        //nie udało się skasować rekordu w bazie
                        Toast.makeText(MainActivity.this, "Error. Product has not been deleted from database.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    };


    @Override
    protected void onDestroy() {
        Log.d(TAG, "MainActivity: onDestroy() unbinding service");
        this.unbindService(mServiceConnection);
        super.onDestroy();
    }

//TODO: deleteProduct Method called from swype on RecyclerView item

}
