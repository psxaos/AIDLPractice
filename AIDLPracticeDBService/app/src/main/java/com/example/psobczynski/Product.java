package com.example.psobczynski;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by p.sobczynski on 2017-09-14.
 */

public class Product implements Parcelable {
    private String name;
    private int price;
    private String country;
    private long delivery;
    private long index;


    public Product(String name, int price, String country, long delivery, long index) {
        this.setName(name);
        this.setPrice(price);
        this.setCountry(country);
        this.setDelivery(delivery);
        this.setIndex(index);
    }

    protected Product(Parcel in) {
        name = in.readString();
        price = in.readInt();
        country = in.readString();
        delivery = in.readLong();
        index = in.readLong();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getDelivery() {
        return delivery;
    }

    public void setDelivery(long delivery) {
        this.delivery = delivery;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeInt(price);
        dest.writeString(country);
        dest.writeLong(delivery);
        dest.writeLong(index);
    }


}
